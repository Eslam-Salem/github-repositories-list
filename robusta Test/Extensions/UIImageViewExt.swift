//
//  UIImageViewExt.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/24/21.
//

import UIKit

struct Cache {
    static let imageCache = NSCache<AnyObject, AnyObject>()
}

extension UIImageView {
    /// firstly check if image is downloaded before load it from cache, if not, download it from server and save it in cache.
    func loadImage(from imagePath: String, completion: @escaping () -> ()) {
        
        if let imageFromCache = Cache.imageCache.object(forKey: imagePath as AnyObject) {
            image = imageFromCache as? UIImage
            completion()
            return
        }
        guard let url = URL(string: imagePath) else {
            completion()
            return
        }
        APIClient.downloadImage(from: url) { data, error in
            completion()
            guard let data = data else { return }
            self.image = UIImage(data: data)
            if let imageToCache = UIImage(data: data) {
                Cache.imageCache.setObject(imageToCache, forKey: imagePath as AnyObject)
            }
        }
    }
}
