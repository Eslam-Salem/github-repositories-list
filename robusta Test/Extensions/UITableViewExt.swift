//
//  UITableViewExt.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import UIKit
import QuickLayout

extension UITableView {
    func dequeCell<Cell: UITableViewCell> () -> Cell {
        let cellName = String(describing: Cell.self)
        guard let myCell = dequeueReusableCell(withIdentifier: cellName) as? Cell else { return Cell() }
        return myCell
    }

    func setEmptyView(message: String, isScrollable: Bool = true) {
        let emptyView = UIView(frame: CGRect(x: 0, y: 0, width: bounds.size.width, height: bounds.size.height / 2))
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont.systemFont(ofSize: 17.5)
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        emptyView.addSubview(messageLabel)
        messageLabel.centerInSuperview()
        if isScrollable {
            tableHeaderView = emptyView
        } else {
            backgroundView = emptyView
        }
    }

    func removeEmptyState(isScrollable: Bool = true) {
        if isScrollable {
            tableHeaderView = nil
        } else {
            backgroundView = nil
        }
    }
}
