//
//  UIViewExt.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import UIKit

extension UIView {
    func setShadow() {
        layer.cornerRadius = 7
        layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        layer.shadowRadius = 10
        layer.shadowOffset = .zero
        layer.shadowOpacity = 1
        layer.masksToBounds = false
    }

    func makeRounded() {
        layer.masksToBounds = true
        layer.cornerRadius = bounds.height / 2
    }
}
