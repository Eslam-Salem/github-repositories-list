//
//  RepositoryDetailsViewController.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import UIKit

class RepositoryDetailsViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    var viewModel: RepositoryDetailsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }

    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension RepositoryDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView()
    }
}

extension RepositoryDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        RepositoryDetailsCells.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentCell = RepositoryDetailsCells.allCases[indexPath.row]
        switch currentCell {
        case .headerInfo:
            return configureHeaderInfoCell()
        default:
            return configureInfoCell(currentCell: currentCell)
        }
    }

    private func configureHeaderInfoCell() -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell = tableView.dequeCell() as RepositoyHeaderDataTableViewCell
        cell.setDataToCell(with: viewModel.repositoryData)
        return cell
    }

    private func configureInfoCell(currentCell: RepositoryDetailsCells) -> UITableViewCell {
        guard let viewModel = viewModel else { return UITableViewCell() }
        let cell = tableView.dequeCell() as RepositoyInfoTableViewCell
        cell.setDataToCell(title: currentCell.title, description: viewModel.getInfoDescription(currentCell: currentCell))
        return cell
    }
}
