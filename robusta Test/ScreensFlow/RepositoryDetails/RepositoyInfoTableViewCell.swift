//
//  RepositoyInfoTableViewCell.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/24/21.
//

import UIKit

class RepositoyInfoTableViewCell: UITableViewCell {
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    
    func setDataToCell(title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
