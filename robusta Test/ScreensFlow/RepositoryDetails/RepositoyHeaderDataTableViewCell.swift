//
//  RepositoyHeaderDataTableViewCell.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/24/21.
//

import UIKit

class RepositoyHeaderDataTableViewCell: UITableViewCell {
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var avatarImageView: UIImageView!
    @IBOutlet private var imageActivityIndicator: UIActivityIndicatorView!
    
    func setDataToCell(with data: RepositoriesListModel) {
        titleLabel.text = data.name
        descriptionLabel.text = data.description
        setAvatarImage(imagePath: data.owner?.avatarUrl ?? "")
    }

    private func setAvatarImage(imagePath: String) {
        imageActivityIndicator.startAnimating()
        avatarImageView.loadImage(from: imagePath) {
            self.imageActivityIndicator.stopAnimating()
        }
    }
}
