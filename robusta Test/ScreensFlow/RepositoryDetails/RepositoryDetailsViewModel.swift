//
//  RepositoryDetailsViewModel.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/24/21.
//

import Foundation

enum RepositoryDetailsCells: CaseIterable {
    case headerInfo
    case ownerName
    case ownerType
    case repoHTML
    case repoURL
    
    var title: String {
        switch self {
        case .headerInfo:
            return ""
        case .ownerName:
            return "Owner Name:"
        case .ownerType:
            return "Owner Type:"
        case .repoHTML:
            return "Repository HTML:"
        case .repoURL:
            return "Repository URL:"
        }
    }
}

class RepositoryDetailsViewModel {
    var repositoryData: RepositoriesListModel
    
    init(repositoryData: RepositoriesListModel) {
        self.repositoryData = repositoryData
    }

    func getInfoDescription(currentCell: RepositoryDetailsCells) -> String {
        switch currentCell {
        case .headerInfo:
            return ""
        case .ownerName:
           return repositoryData.owner?.userName ?? ""
        case .ownerType:
            return repositoryData.owner?.type ?? ""
        case .repoHTML:
            return repositoryData.repoHtml ?? ""
        case .repoURL:
            return repositoryData.repoUrl ?? ""
        }
    }
}
