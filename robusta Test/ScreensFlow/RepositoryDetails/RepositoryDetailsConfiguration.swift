//
//  RepositoryDetailsConfigurator.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/24/21.
//

import UIKit

enum RepositoryDetailsConfiguration {
    static func navigateToRepositoryDetails(repository: RepositoriesListModel, presentingViewController: UIViewController) {
        guard let detailsViewCtrl = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RepositoryDetailsViewController") as? RepositoryDetailsViewController else { return }
        detailsViewCtrl.viewModel = RepositoryDetailsViewModel(repositoryData: repository)
        presentingViewController.present(detailsViewCtrl, animated: true)
    }
}
