//
//  RepositoriesListViewModel.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import Foundation

enum currentContext {
    case searching(keyword: String)
    case viewAllData
}

class RepositoriesListViewModel {
    var errorHandler: ((_: String) -> Void)?
    var newDataToDisplayHandler: (() -> Void)?
    var dataSource = [RepositoriesListModel]()
    var currentContext: currentContext = .viewAllData {
        didSet {
            generateDataSource()
        }
    }
    var allRepositories = [RepositoriesListModel]()
    
    //MARK: -  functions

    func requestReposotoriesList() {
        guard let url = URL(string: RepositoriesAPIServices.repositoriesList.rawValue) else {
            errorHandler?("Something went worng!!!")
            return
        }
        APIClient.request(url: url, httpMethod: .get, responseType: [RepositoriesListModel].self) { [weak self] response, error in
            guard let response = response, error == nil else {
                self?.errorHandler?(error?.localizedDescription ?? "")
                return
            }
            self?.allRepositories = response
            self?.generateDataSource()
            self?.newDataToDisplayHandler?()
        }
    }

    func generateDataSource() {
        dataSource.removeAll()
        switch currentContext {
        case .searching(let keyword):
            dataSource = allRepositories.filter {
                $0.name.lowercased().contains(keyword.lowercased())
            }
        case .viewAllData:
            dataSource = allRepositories
        }
        newDataToDisplayHandler?()
    }
}
