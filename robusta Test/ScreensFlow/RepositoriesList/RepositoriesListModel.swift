//
//  RepositoriesListModel.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import Foundation

struct RepositoryOwner: Decodable {
    private enum CodingKeys: String, CodingKey {
        case userName = "login"
        case avatarUrl = "avatar_url"
        case type
    }
    var avatarUrl: String
    var userName: String
    var type: String
}

struct RepositoriesListModel: Decodable, Equatable {
    static func == (lhs: RepositoriesListModel, rhs: RepositoriesListModel) -> Bool {
        lhs.id == rhs.id
    }
    
    private enum CodingKeys: String, CodingKey {
        case id,owner,description,name
        case repoUrl = "url"
        case repoHtml = "html_url"
    }
    var id: Int?
    var owner: RepositoryOwner?
    var name: String
    var description: String?
    var repoUrl: String?
    var repoHtml: String?
}
