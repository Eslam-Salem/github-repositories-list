//
//  RepositoriesListViewController.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import UIKit

class RepositoriesListViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var searchBar: UISearchBar!
    private let viewModel = RepositoriesListViewModel()
    private var activityIndicator = ActivityIndicator()
    private var alertView: AlertHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAlert()
        configureTableView()
        bindViewModel()
        requestReposotoriesList()
        configureSearchBar()
    }

    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func configureSearchBar() {
        searchBar.delegate = self
    }

    private func configureAlert() {
        alertView = AlertHandler(presentingViewCtrl: self)
    }

    private func requestReposotoriesList() {
        activityIndicator.displayForLoading(in: view)
        viewModel.requestReposotoriesList()
    }

    private func bindViewModel() {
        viewModel.newDataToDisplayHandler = { [weak self] in
            self?.activityIndicator.dismiss()
            self?.handleEmptyState()
            self?.tableView.reloadData()
        }
        viewModel.errorHandler = { [weak self] message in
            self?.activityIndicator.dismiss()
            self?.alertView?.showErrorMessage(message: message)
        }
    }

    private func handleEmptyState() {
        if viewModel.dataSource.isEmpty {
            self.tableView.setEmptyView(message: "No Repositories found")
        } else {
            self.tableView.removeEmptyState()
        }
    }
}

extension RepositoriesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RepositoryDetailsConfiguration.navigateToRepositoryDetails(
            repository: viewModel.dataSource[indexPath.row],
            presentingViewController: self
        )
    }
}

extension RepositoriesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeCell() as RepositoryTableViewCell
        cell.setDataToCell(with: viewModel.dataSource[indexPath.row])
        return cell
    }
}

extension RepositoriesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        /// check if text contains at least 2 characters to search for matching
        if searchText.isEmpty || searchText.count < 2 {
            viewModel.currentContext = .viewAllData
        } else {
            viewModel.currentContext = .searching(keyword: searchText)
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}
