//
//  RepositoryTableViewCell.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var ownerNameLabel: UILabel!
    @IBOutlet private var avatarImageView: UIImageView!
    @IBOutlet private var imageActivityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureDesign()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageActivityIndicator.isHidden = true
        avatarImageView.image = nil
    }

    func setDataToCell(with data: RepositoriesListModel) {
        titleLabel.text = data.name
        descriptionLabel.text = data.description
        if let userName = data.owner?.userName {
            ownerNameLabel.text = "By: \(userName)"
        }
        setAvatarImage(imagePath: data.owner?.avatarUrl ?? "")
    }

    private func setAvatarImage(imagePath: String) {
        imageActivityIndicator.startAnimating()
        avatarImageView.loadImage(from: imagePath) {
            self.imageActivityIndicator.stopAnimating()
        }
    }

    private func configureDesign() {
        containerView.setShadow()
        avatarImageView.makeRounded()
    }
}
