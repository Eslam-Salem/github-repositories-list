//
//  RepositoriesAPIServices.swift
//  robusta Test
//
//  Created by Eslam Salem on 4/23/21.
//

import Foundation

enum RepositoriesAPIServices: String {
    case repositoriesList = "https://api.github.com/repositories"
}
