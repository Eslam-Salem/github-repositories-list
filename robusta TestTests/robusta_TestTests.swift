//
//  robusta_TestTests.swift
//  robusta TestTests
//
//  Created by Eslam Salem on 4/24/21.
//

import XCTest
@testable import robusta_Test

class robusta_TestTests: XCTestCase {

    let alamofireRepo = RepositoriesListModel(name: "alamofireRepo")
    let kingFisherRepo = RepositoriesListModel(name: "kingFisherRepo")
    let SDWebImageRepo = RepositoriesListModel(name: "SDWebImageRepo")
    let viewModel = RepositoriesListViewModel()

    override func setUp() {
        super.setUp()
        viewModel.allRepositories = [alamofireRepo,kingFisherRepo,SDWebImageRepo]
        viewModel.dataSource = [alamofireRepo,kingFisherRepo,SDWebImageRepo]
    }

    override func tearDown() {
        viewModel.dataSource = []
        viewModel.allRepositories = []
        super.tearDown()
    }
    
    func testViewAllMode() {
        viewModel.currentContext = .viewAllData
        viewModel.generateDataSource()
        XCTAssertTrue(viewModel.dataSource == [alamofireRepo,kingFisherRepo,SDWebImageRepo])
    }

    func testSearchingTrueWholeWord() {
        viewModel.currentContext = .searching(keyword: "kingFisherRepo")
        viewModel.generateDataSource()
        XCTAssertTrue(viewModel.dataSource == [kingFisherRepo])
    }

    func testSearchingTrueSubset() {
        viewModel.currentContext = .searching(keyword: "web")
        viewModel.generateDataSource()
        XCTAssertTrue(viewModel.dataSource == [SDWebImageRepo])
    }

    func testSearchingWrongWord() {
        viewModel.currentContext = .searching(keyword: "Moya")
        viewModel.generateDataSource()
        XCTAssertTrue(viewModel.dataSource == [])
    }
    
    func testSearchingWrongSubset() {
        viewModel.currentContext = .searching(keyword: "kb")
        viewModel.generateDataSource()
        XCTAssertFalse(viewModel.dataSource == [SDWebImageRepo])
    }
}
